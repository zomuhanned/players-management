<?php

namespace App\Enums;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Collection;

enum Positions: string {
    case defender = 'defender';
    case midfielder = 'midfielder';
    case forward = 'forward';


    public static function getValues(): collection{
        return collect(self::cases())->pluck('value');
    }

    public static function haveValue(string $value): bool{
        return self::getValues()->contains($value);
    }

}