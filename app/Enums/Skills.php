<?php

namespace App\Enums;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Collection;

enum Skills: string {
    case defense = 'defense';
    case attack = 'attack';
    case speed = 'speed';
    case strength = 'strength';
    case stamina = 'stamina';


    public static function getValues(): collection{
        return collect(self::cases())->pluck('value');
    }

    public static function haveValue(string $value): bool{
        return self::getValues()->contains($value);
    }

}