<?php

// /////////////////////////////////////////////////////////////////////////////
// PLEASE DO NOT RENAME OR REMOVE ANY OF THE CODE BELOW. 
// YOU CAN ADD YOUR CODE TO THIS FILE TO EXTEND THE FEATURES TO USE THEM IN YOUR WORK.
// /////////////////////////////////////////////////////////////////////////////

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\PlayerSkill;
use App\Enums\Positions;
use App\Enums\Skills;
use Exception;
use Illuminate\Support\Facades\Validator;

class PlayerController extends Controller
{
    public function index()
    {
        $data = Player::with('playerSkills')->get();
        return response($data, 200);
    }

    public function show($id)
    {
        $player = Player::find($id);
        if(!$player){
            return response('Player is not found', 500);
        }
        $player = Player::with('playerSkills')->find($id);
        return response($player, 200);
    }

    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'position' => 'required',
                'playerSkills' => 'required|array'
            ]);
            if ($validator->fails()) {
                return response($validator->errors());
            }
            if(count($request->playerSkills) == 0){
                return response('Player must have at least one skill');
            }
            if(!Positions::haveValue($request->position)){
                return response('Invalid value for position: '.$request->position, 500);
            }
            foreach($request->playerSkills as $skill){
                if(!Skills::haveValue($skill['skill'])){
                    return response('Invalid value for skill: '.$skill['skill'], 500);
                }
            } 
            $occurences = array_count_values(array_column($request->playerSkills, 'skill'));
            foreach($occurences as $key => $value){
                if($value > 1){
                    return response('Duplicated Skill: '.$key, 500);
                }
            }
            $id= Player::create([
                'name' => $request->name,
                'position' => $request->position
            ])->id;
            foreach($request->playerSkills as $skill){
                PlayerSkill::create([
                    'player_id' => $id,
                    'skill' => $skill['skill'],
                    'value' => $skill['value']
                ]);
            }
            $player = Player::with('playerSkills')->find($id);
            return response($player, 200);
        }catch(Exception $e){
            return response($e.getMessage(), 500);
        }
    }

    public function update(Request $request, $id){
        try{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'position' => 'required',
                'playerSkills' => 'required|array'
            ]);
            if ($validator->fails()) {
                return response($validator->errors());
            }
            $player = Player::find($id);
            if(!$player){
                return response('Player is not found', 500);
            }
            if(count($request->playerSkills) == 0){
                return response('Player must have at least one skill', 500);
            }
            if(!Positions::haveValue($request->position)){
                return response('Invalid value for position: '.$request->position, 500);
            }
            foreach($request->playerSkills as $skill){
                if(!Skills::haveValue($skill['skill'])){
                    return response('Invalid value for skill: '.$skill['skill'], 500);
                }
            }
            $occurences = array_count_values(array_column($request->playerSkills, 'skill'));
            foreach($occurences as $key => $value){
                if($value > 1){
                    return response('Duplicated Skill: '.$key, 500);
                }
            }
            PlayerSkill::where('player_id',$id)->delete();
            $player->update([
                'name' => $request->name,
                'position' => $request->position
            ]);   
            foreach($request->playerSkills as $skill){
                PlayerSkill::create([
                    'player_id' => $id,
                    'skill' => $skill['skill'],
                    'value' => $skill['value']
                ]);
            } 
            $player = Player::with('playerSkills')->find($id);
            return response($player, 200);
        }catch(Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function destroy(Request $request, $id)
    {
        $token = $request->bearerToken();
        if($token != 'SkFabTZibXE1aE14ckpQUUxHc2dnQ2RzdlFRTTM2NFE2cGI4d3RQNjZmdEFITmdBQkE='){
            return response('Unathorized', 500);
        }
        $player = Player::find($id);
        if(!$player){
            return response('Player is not found', 500);
        }
        PlayerSkill::where('player_id', $id)->delete();
        $player->delete();
        return response("Deleted", 200);
    }
}
