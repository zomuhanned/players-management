<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\PlayerSkill;
use App\Enums\Positions;
use App\Enums\Skills;
use Illuminate\Support\Facades\Validator;
use Exception;

class TeamController extends Controller
{
    //  
    public function selectTeam(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $occurences = array_count_values(array_column($data, 'position'));
            foreach($occurences as $key => $value){
                if($value > 1){
                    return response('Duplicated Position: '.$key, 500);
                }
            }
            foreach($data as $q){
                if(!Positions::haveValue($q['position'])){
                    return response('Invalid value for position: '.$q['position'], 500);
                }
                if(!Skills::haveValue($q['mainSkill'])){
                    return response('Invalid value for position: '.$q['mainSkill'], 500);
                }
            }
            $result = array();
            foreach($data as $d){
                if($d['numberOfPlayers'] <= 0){
                    return response('invalid value for number of players');
                }
                $skill = $d['mainSkill'];
                $players = Player::join('player_skills', 'players.id', '=', 'player_skills.player_id')
                ->select('players.*')
                ->with('playerSkills',function($q) use ($skill){
                    $q->where('skill', $skill);
                })
                ->where('players.position', $d['position'])
                ->where('player_skills.skill', $d['mainSkill'])
                ->orderBy('player_skills.value', 'desc')
                ->take($d['numberOfPlayers'])
                ->get();
                if($players->count() != 0){
                    if($players->count() < $d['numberOfPlayers']){
                        return response('insufficiant number of players for position: '.$d['position']);
                    }else{
                        foreach($players as $player){
                            array_push($result, $player);
                        }
                    }
                }else{
                    $players = Player::join('player_skills', 'players.id', '=', 'player_skills.player_id')
                    ->select('players.*')
                    ->where('players.position', $d['position'])
                    ->with('playerSkills')
                    ->orderBy('player_skills.value', 'desc')
                    ->get();
                    $mid = array();
                    foreach($players as $player){
                        $found_key = array_search($player->id, array_column($mid, 'id'));
                        $found_key++;
                        if($found_key == false){
                            array_push($mid, $player);
                        }
                    }
                    if(count($mid) < $d['numberOfPlayers']){
                        return response('insufficiant number of players for position: '.$d['position']);
                    }
                    for($i=0; $i<$d['numberOfPlayers']; $i++){
                        array_push($result,$mid[$i]);
                    }
                }
            }
            return response($result, 200);
        }catch(Exception $e){
            return response($e.getMessage(), 500);
        }
    }


}
